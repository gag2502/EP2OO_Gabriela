package EP2OO_Gabriela.src;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.FileInputStream;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;


@SuppressWarnings("serial")
public class EPGabriela extends javax.swing.JFrame {

   
    public EPGabriela() {
        initComponents();
    }
                           
    private void initComponents() {

    	 fileChooser = new javax.swing.JFileChooser();
         Jp = new javax.swing.JPanel();
         Jl1 = new javax.swing.JLabel();
         jlabel1 = new javax.swing.JLabel();
         Jl2 = new javax.swing.JLabel();
         jlabel2 = new javax.swing.JLabel();
         jSeparator1 = new javax.swing.JSeparator();
         jComboBox1 = new javax.swing.JComboBox<>();
         jMenuBar1 = new javax.swing.JMenuBar();
         jMenu1 = new javax.swing.JMenu();
         Open = new javax.swing.JMenuItem();
         Exit = new javax.swing.JMenuItem();

         fileChooser.setFileFilter(new MyCustomFilter());

         setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
         setTitle("EP2 Gabriela Alves ");

         javax.swing.GroupLayout JpLayout = new javax.swing.GroupLayout(Jp);
         Jp.setLayout(JpLayout);
         JpLayout.setHorizontalGroup(
             JpLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
             .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, JpLayout.createSequentialGroup()
                 .addGap(0, 0, Short.MAX_VALUE)
                 .addComponent(Jl1, javax.swing.GroupLayout.PREFERRED_SIZE, 512, javax.swing.GroupLayout.PREFERRED_SIZE)
                 .addGap(10, 10, 10))
         );
         JpLayout.setVerticalGroup(
             JpLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
             .addComponent(Jl1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
         );

         jlabel1.setText("Imagem Original");

         jlabel2.setText("Imagem com Filtro");

         jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] {  }));
         
         jMenu1.setText("File");

         Open.setText("Open");
         Open.addActionListener(new java.awt.event.ActionListener() {
             public void actionPerformed(java.awt.event.ActionEvent evt) {
                 OpenActionPerformed(evt);
             }
         });
         jMenu1.add(Open);

         Exit.setText("Exit");
         Exit.addActionListener(new java.awt.event.ActionListener() {
             public void actionPerformed(java.awt.event.ActionEvent evt) {
                 ExitActionPerformed(evt);
             }
         });
         jMenu1.add(Exit);

         jMenuBar1.add(jMenu1);

         setJMenuBar(jMenuBar1);

         javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
         getContentPane().setLayout(layout);
         layout.setHorizontalGroup(
             layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
             .addGroup(layout.createSequentialGroup()
                 .addGap(36, 36, 36)
                 .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                     .addGroup(layout.createSequentialGroup()
                         .addComponent(jlabel1)
                         .addGap(329, 329, 329))
                     .addGroup(layout.createSequentialGroup()
                         .addComponent(Jp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                         .addGap(10, 10, 10)
                         .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                 .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                     .addGroup(layout.createSequentialGroup()
                         .addGap(10, 10, 10)
                         .addComponent(Jl2, javax.swing.GroupLayout.PREFERRED_SIZE, 527, javax.swing.GroupLayout.PREFERRED_SIZE)
                         .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                     .addGroup(layout.createSequentialGroup()
                         .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                         .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                         .addGap(146, 146, 146)
                         .addComponent(jlabel2)
                         .addGap(245, 245, 245))))
         );
         layout.setVerticalGroup(
             layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
             .addGroup(layout.createSequentialGroup()
                 .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                     .addGroup(layout.createSequentialGroup()
                         .addGap(36, 36, 36)
                         .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                     .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                         .addContainerGap()
                         .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                             .addComponent(jlabel1)
                             .addComponent(jlabel2)
                             .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                         .addGap(3, 3, 3)
                         .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                             .addComponent(Jl2, javax.swing.GroupLayout.DEFAULT_SIZE, 543, Short.MAX_VALUE)
                             .addComponent(Jp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                 .addContainerGap(29, Short.MAX_VALUE))
         );

         pack();
    }                       

    private void OpenActionPerformed(java.awt.event.ActionEvent evt) {                                     
       
    int returnVal = fileChooser.showOpenDialog(this);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
    	
        String caminho = fileChooser.getSelectedFile().getAbsolutePath();
     
       try {
			FileInputStream arquivo = new FileInputStream(caminho);
			BufferedImage imagem_pgm = null;
			BufferedImage imagem_ppm = null;
			BufferedImage imagem_pgm_negativo = null;
		//	BufferedImage imagem_pgm_smooth = null;
			//int sizeSm = 3;
			//int divSm = 9;
			//int matriz[] = {1, 1, 1, 1, 1, 1, 1, 1, 1};
			//int value, i, j;
		    int largura = 0 ;
		    int altura = 0 ;
		    int valorMaximo = 0;
			int count = 0 ;
			byte bytes = 0;
			LerPGM lerPGM = new LerPGM ();
			LerPPM lerPPM = new LerPPM();
			
			
			String linha = LerPGM.le_linha(arquivo);
			if("P5".equals(linha)) {
			lerPGM.leitura(linha, lerPGM.getNumeroMagico(), arquivo, valorMaximo, altura, largura, count);
			
			imagem_pgm = new BufferedImage(lerPGM.getLargura(), lerPGM.getAltura(), BufferedImage.TYPE_BYTE_GRAY);
			imagem_pgm_negativo = new BufferedImage(lerPGM.getLargura(), lerPGM.getAltura(), BufferedImage.TYPE_BYTE_GRAY);
		//	imagem_pgm_smooth = new BufferedImage(lerPGM.getLargura(), lerPGM.getAltura(), BufferedImage.SCALE_SMOOTH);
			byte [] pixels = ((DataBufferByte) imagem_pgm.getRaster().getDataBuffer()).getData();
			byte [] pixels_negativo = ((DataBufferByte) imagem_pgm_negativo.getRaster().getDataBuffer()).getData();
		//	byte [] pixels_smooth = ((DataBufferByte) imagem_pgm_smooth.getRaster().getDataBuffer()).getData();
			
			while(count < (lerPGM.getAltura()*lerPGM.getLargura())) {
				
				bytes = (byte) arquivo.read();
				pixels[count] = bytes;
				pixels_negativo[count] = (byte) (valorMaximo - bytes); 
				count++;
			}
			
		
			/*for(i=0; i< sizeSm; i++){  
			for(j=0; j<largura; j++){
				pixels_smooth[i+j*largura] = pixels[i+j*largura];  
				pixels_smooth[(i+j*largura)+largura-1] = pixels[(i+j*largura)+largura-1];
			}
		}
	  
		for(i=0; i<sizeSm; i++){
			for(j=0; j<altura; j++){
	  
				pixels_smooth[i*altura+j] = pixels[i*altura+j];
				pixels_smooth[(i+j*largura)+largura-1] = pixels[(i+j*largura)+largura-1];
			}
				}
			   
				for(i=1; i<largura-1; i++){
					for(j=1; j<altura-1; j++){
			 
						value = 0;
						
						for(int x=-(1); x<=1; x++){
							for(int y=-(1); y<=1;y++){
			  
								value += matriz[(x+1)+sizeSm*(y+1)]*(char)pixels[(i+x)+(y+j)*largura];
			 }
			  			}
			  
							value /= divSm;
							value = value < 0 ? 0 : value;
							value = value > 255 ? 255 : value;
							
							pixels_smooth[i+j*largura] = (byte) value;
					}
				}*/
			
			jComboBox1.removeAllItems();
			Jl2.setIcon(null); 
			
			ImageIcon icon1 = new ImageIcon(imagem_pgm);
			final ImageIcon icon2 = new ImageIcon(imagem_pgm_negativo);
			//final ImageIcon icon3 = new ImageIcon(imagem_pgm_smooth);
                        
						
                        Jp.setBackground(null);
                        Jl1.setIcon(icon1);
                        
                        jComboBox1.addItem("--Escolha um Item--");
                        jComboBox1.addItem("Filtro Negativo");
                        jComboBox1.addItem("Filtro SMOOTH");
                        jComboBox1.addItem("Filtro SHARPEN");
               
                        jComboBox1.addActionListener(
                        	      new ActionListener(){
                        	        public void actionPerformed(ActionEvent e){
                        	          String valor = (String)(jComboBox1.getSelectedItem());
                        	          if (valor == "Filtro Negativo")
                        	        	  Jl2.setIcon(icon2); 
                        	          else if (valor == "Filtro SMOOTH"){ 
                        	        	 // Jl2.setIcon(icon3); 
                        	          }
                        	          else if (valor == "Filtro SHARPEN"){ 
                         	        	 // Jl2.setIcon(icon4); 
                         	          }
                        	        }
                        	      }
                        	    );
                           
       }
			
			
                        else  if("P6".equals(linha)) {
                        	lerPPM.leitura(linha, lerPPM.getNumeroMagico(), arquivo, valorMaximo ,altura ,largura, count);
            				
            				
            				imagem_ppm = new BufferedImage(lerPPM.getLargura(), lerPPM.getAltura(), BufferedImage.TYPE_INT_RGB);
            				
            				ImageIcon icon5 = new ImageIcon(imagem_ppm);
            				
            							jComboBox1.removeAllItems();
            							jComboBox1.addItem("--Escolha um Item--");
            							jComboBox1.addItem("Filtro Vermelho");
            							jComboBox1.addItem("Filtro Verde");
            							jComboBox1.addItem("Filtro Azul");
            							
            							Jp.setBackground(null);
            	                        Jl1.setIcon(icon5);
            	                        Jl2.setIcon(null);
                        }
						  
						
            			else {
            				JOptionPane.showMessageDialog(null, "Arquivo Inválido! Escolha apenas Imagens .pgm ou .ppm");
            			}

                        
			arquivo.close();
		}
		catch(Throwable t) {
			t.printStackTrace(System.err) ;
			
		}
       
    }                                    
    }
    private void ExitActionPerformed(java.awt.event.ActionEvent evt) {                                     
        System.exit(0);
    }                                    
    
    public static void main(String args[]) {
      
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EPGabriela.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EPGabriela.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EPGabriela.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EPGabriela.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        

        /* Cria e torna visível o JFrame */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EPGabriela().setVisible(true);
            }
        });
    }

                       
    private javax.swing.JMenuItem Exit;
    private javax.swing.JLabel Jl1;
    private javax.swing.JLabel Jl2;
    private javax.swing.JPanel Jp;
    private javax.swing.JMenuItem Open;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel jlabel1;
    private javax.swing.JLabel jlabel2;
                       
}
